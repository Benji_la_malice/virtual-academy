<?php

function lire_infos_exercices($nom_utilisateur) {

	$pdo = PDO2::getInstance();

	$requete = $pdo->prepare("SELECT nb_exercice_realiser_type1, nb_exercice_reussi_type1,nb_exercice_realiser_type2, nb_exercice_reussi_type2,nb_exercice_realiser_type3, nb_exercice_reussi_type3,nb_exercice_realiser_type4, nb_exercice_reussi_type4
		FROM info_exo
		WHERE
		nom_utilisateur = :nom_utilisateur");

	$requete->bindValue(':nom_utilisateur', $nom_utilisateur);
	$requete->execute();
	
	if ($result = $requete->fetch(PDO::FETCH_ASSOC)) {
	
		$requete->closeCursor();
		return $result;
	}
	return false;
}

?>



