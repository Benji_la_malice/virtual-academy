<?php

// Vérification des droits d'accès de la page
if (!utilisateur_est_connecte()) {

	// On affiche la page d'erreur comme quoi l'utilisateur doit être connecté pour voir la page
	include CHEMIN_VUE_GLOBALE.'erreur_non_connecte.php';
	
}
else {

	// On veut utiliser le modèle des exercices (~/modules/exercices.php)
	include CHEMIN_MODELE.'exercices.php';
	
	// lire_infos_exercices() est défini dans ~/modules/exercices.php
	$infos_exercices = lire_infos_exercices($_SESSION['pseudo']);
	
	// Si les infos existes
	if (!false == $infos_exercices) {

		$nb_exercice_realiser_type1=$infos_exercices['nb_exercice_realiser_type1'];
		$nb_exercice_reussi_type1=$infos_exercices['nb_exercice_reussi_type1'];
		
		$nb_exercice_realiser_type2=$infos_exercices['nb_exercice_realiser_type2'];
		$nb_exercice_reussi_type2=$infos_exercices['nb_exercice_reussi_type2'];
		
		$nb_exercice_realiser_type3=$infos_exercices['nb_exercice_realiser_type3'];
		$nb_exercice_reussi_type3=$infos_exercices['nb_exercice_reussi_type3'];
		
		$nb_exercice_realiser_type4=$infos_exercices['nb_exercice_realiser_type4'];
		$nb_exercice_reussi_type4=$infos_exercices['nb_exercice_reussi_type4'];
		
		include CHEMIN_VUE.'monitoring_utilisateur.php';
	} 
	else {

		include CHEMIN_VUE.'erreur_info_exo.php';
	}
}
?>