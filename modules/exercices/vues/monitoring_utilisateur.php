<h2> Page de suivi du membre <?php echo htmlspecialchars($_SESSION['pseudo']); ?></h2>
</br>
<table>
	<tr>
		<th>Mathématique</th>
		<th>Matière 1</th>
		<th>Matière 2</th>
		<th>Matière 3</th>
		<th>Matière 4</th>
	</tr>
	<tr>
		<th>Nombre d'exercices réalisés</th>
		<td><?php echo $nb_exercice_realiser_type1; ?></td>
		<td><?php echo $nb_exercice_realiser_type2; ?></td>
		<td><?php echo $nb_exercice_realiser_type3; ?></td>
		<td><?php echo $nb_exercice_realiser_type4; ?></td>
	</tr>
	<tr>
		<th>Nombre d'exercices réussis</th>
		<td><?php echo $nb_exercice_reussi_type1; ?></td>
		<td><?php echo $nb_exercice_reussi_type2; ?></td>
		<td><?php echo $nb_exercice_reussi_type3; ?></td>
		<td><?php echo $nb_exercice_reussi_type4; ?></td>
	</tr>
</table>
