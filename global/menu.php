<div id="menu">
	
		<h2>Menu</h2>
		
		<ul>
			<li><a href="index.php">Accueil</a></li>
		</ul>
		
		<h3>Espace membre</h3>

		<?php if (!utilisateur_est_connecte()) { ?>
		<ul>
			<li><a href="index.php?module=membres&amp;action=inscription">Inscription</a></li>
			<li><a href="index.php?module=membres&amp;action=connexion">Connexion</a></li>
		</ul>
		<?php } else { ?>
		<p>Bienvenue, <?php echo htmlspecialchars($_SESSION['pseudo']); ?>.</p>
		<ul>
			<li><a href="index.php?module=membres&amp;action=deconnexion">Déconnexion</a></li>
		</ul>
		<ul>
			<li> <?php echo '<a href="index.php?module=membres&amp;action=afficher_profil&id='.htmlspecialchars($_SESSION['id']).'">'.'Mon profil</a> '; ?> </li>
		</ul>
		<ul>
			<li><a href="index.php?module=membres&amp;action=modifier_profil">Modifier son profil</a></li>
		</ul>
		<ul>
			<li><a href="index.php?module=exercices&amp;action=afficher_monitoring">Suivi exercices</a></li>
		</ul>
		<ul>
			<li><a href="index.php?module=exercices&amp;action=afficher_calculette">Calculette</a></li>
		</ul>
		<?php } ?>

	
	</div>